<?php
/**
 * @file
 * Caravan's theme implementation to display a single Drupal page.
 *
*/
?>
  <div class="wrapper">
    <div class="container">
      <header class="header">
        <div class="header-logo">
         <a href="/"><img src="/<?php print path_to_theme() ; ?>/images/logo.png" alt="<?php print variable_get("site_name",'Caravan'); ?>" width="160" height="11"></a>
        </div>
        <?php print render($page['header']); ?>
      </header>
      <!-- header -->
      
	  <nav>
       <?php print render($page['navigation']); ?>
      </nav>
      <!-- nav -->
      
      <?php print $breadcrumbs; ?>
      <!-- breadcrumbs -->
      
      <main class="main" id="content" aria-label="Main Content">
      <?php print $messages; ?>
      <?php print render($page['content_top']); ?>
      <?php print render($page['content']); ?>
      <?php print render($page['content_bottom']); ?>
      </main>
      <!-- main -->      
    </div>
      <footer class="footer">
      <?php print render($page['footer']); ?>
      </footer>
      <!-- footer -->
  </div>
  <!-- wrapper -->