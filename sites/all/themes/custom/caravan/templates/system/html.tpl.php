<?php
/**
 * @file
 * Returns the HTML for the basic html structure of a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728208
*/
?>
<!doctype html>
<!--[if IE 8]> <html class="no-js ie8" lang="<?php print $GLOBALS['language']->language; ?>"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="<?php print $GLOBALS['language']->language; ?>">
<!--<![endif]-->

<head>
<?php print $head; ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php print $head_title; ?></title>
<?php print $styles; ?>
<?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
<?php print $page_top; ?>
<?php print $page; ?>
<?php print $page_bottom; ?>
</body>
</html>
