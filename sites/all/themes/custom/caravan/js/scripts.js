(function($) {
  $(document).ready(function(e) {
    $('.cta .grid .grid-unit.grid-unit-1-2.grid-unit-ll-gap:nth-child(even)').each(function(index,element){
	  $(element).removeClass('grid-unit-ll-gap');
	  $(element).addClass('grid-unit-rl-gap');
    });
  });
})(jQuery);