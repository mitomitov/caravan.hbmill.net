<?php
/**
 * @file
 * Preprocess : page
 */



/**
 * Implements template_preprocess_page().
 */

function caravan_preprocess_page(&$variables) {
  global $theme_path;
  $breadcrumbs = '<div id="breadcrumbs">';
  $breadcrumbs.= '<strong>'.t("You are here").':</strong> '.((drupal_is_front_page())?t("Frontpage"):'').'  |  '.((!empty($variables['node']))?$variables['node']->title:drupal_get_title()).'';
  $breadcrumbs.= '<a href="/print/'.drupal_get_path_alias().'" class="print_icon"  target="_blank"><img src="/'.$theme_path.'/images/print_icon.png" alt="'.t("Print").'">'.t("Print").'</a>';
  $breadcrumbs.= '</div>';
  $variables['breadcrumbs'] = $breadcrumbs;
  
}

