<?php
/**
 * @file
 * caravan.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function caravan_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view__panel_context_f6d858e4-2608-4344-9536-8e41878073ec';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Landing page',
    'no_blocks' => FALSE,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
    'panels_page_title_state' => 1,
    'panels_page_title' => 'Om Thiss Technology',
  );
  $display = new panels_display();
  $display->layout = 'threecol_caravan';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'middle' => NULL,
      'right' => NULL,
      'bottom' => NULL,
      'left_bottom' => NULL,
      'middle_bottom_first' => NULL,
      'middle_bottom_second' => NULL,
      'right_bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Home page';
  $display->uuid = 'fc9038c2-f783-43a5-b45d-b4db5b7b8765';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-73cc13e9-c9c8-404f-b109-46b683b723ca';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-secondary';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '73cc13e9-c9c8-404f-b109-46b683b723ca';
  $display->content['new-73cc13e9-c9c8-404f-b109-46b683b723ca'] = $pane;
  $display->panels['left'][0] = 'new-73cc13e9-c9c8-404f-b109-46b683b723ca';
  $pane = new stdClass();
  $pane->pid = 'new-52688648-3c19-4d8b-b28e-75dc3781a534';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'menu-menu-top';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '52688648-3c19-4d8b-b28e-75dc3781a534';
  $display->content['new-52688648-3c19-4d8b-b28e-75dc3781a534'] = $pane;
  $display->panels['left'][1] = 'new-52688648-3c19-4d8b-b28e-75dc3781a534';
  $pane = new stdClass();
  $pane->pid = 'new-93db4601-1c5c-4da1-9847-4f081cb6d4f0';
  $pane->panel = 'left';
  $pane->type = 'block';
  $pane->subtype = 'views-events-block_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Upcoming Events',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '93db4601-1c5c-4da1-9847-4f081cb6d4f0';
  $display->content['new-93db4601-1c5c-4da1-9847-4f081cb6d4f0'] = $pane;
  $display->panels['left'][2] = 'new-93db4601-1c5c-4da1-9847-4f081cb6d4f0';
  $pane = new stdClass();
  $pane->pid = 'new-be65d687-8c36-4a8d-a511-36f375352735';
  $pane->panel = 'left_bottom';
  $pane->type = 'fieldable_panels_pane';
  $pane->subtype = 'fpid:2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'be65d687-8c36-4a8d-a511-36f375352735';
  $display->content['new-be65d687-8c36-4a8d-a511-36f375352735'] = $pane;
  $display->panels['left_bottom'][0] = 'new-be65d687-8c36-4a8d-a511-36f375352735';
  $pane = new stdClass();
  $pane->pid = 'new-4e993f02-72e0-4111-bd42-d88cd93c51c3';
  $pane->panel = 'middle';
  $pane->type = 'node_content';
  $pane->subtype = 'node_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'links' => 0,
    'no_extras' => 0,
    'override_title' => 0,
    'override_title_text' => '',
    'identifier' => '',
    'link' => 0,
    'leave_node_title' => 1,
    'build_mode' => 'full',
    'context' => 'argument_entity_id:node_1',
    'override_title_heading' => 'h1',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '4e993f02-72e0-4111-bd42-d88cd93c51c3';
  $display->content['new-4e993f02-72e0-4111-bd42-d88cd93c51c3'] = $pane;
  $display->panels['middle'][0] = 'new-4e993f02-72e0-4111-bd42-d88cd93c51c3';
  $pane = new stdClass();
  $pane->pid = 'new-22abdd89-922d-4d3b-be5f-e5ff94425ad5';
  $pane->panel = 'middle_bottom_first';
  $pane->type = 'fieldable_panels_pane';
  $pane->subtype = 'fpid:3';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '22abdd89-922d-4d3b-be5f-e5ff94425ad5';
  $display->content['new-22abdd89-922d-4d3b-be5f-e5ff94425ad5'] = $pane;
  $display->panels['middle_bottom_first'][0] = 'new-22abdd89-922d-4d3b-be5f-e5ff94425ad5';
  $pane = new stdClass();
  $pane->pid = 'new-c37f3f9d-8380-4522-9740-1d603c3a8b2b';
  $pane->panel = 'middle_bottom_second';
  $pane->type = 'fieldable_panels_pane';
  $pane->subtype = 'fpid:4';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'c37f3f9d-8380-4522-9740-1d603c3a8b2b';
  $display->content['new-c37f3f9d-8380-4522-9740-1d603c3a8b2b'] = $pane;
  $display->panels['middle_bottom_second'][0] = 'new-c37f3f9d-8380-4522-9740-1d603c3a8b2b';
  $pane = new stdClass();
  $pane->pid = 'new-d20c56b5-5a7a-4336-8f6c-408eecf5ea3e';
  $pane->panel = 'right';
  $pane->type = 'fieldable_panels_pane';
  $pane->subtype = 'fpid:7';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd20c56b5-5a7a-4336-8f6c-408eecf5ea3e';
  $display->content['new-d20c56b5-5a7a-4336-8f6c-408eecf5ea3e'] = $pane;
  $display->panels['right'][0] = 'new-d20c56b5-5a7a-4336-8f6c-408eecf5ea3e';
  $pane = new stdClass();
  $pane->pid = 'new-6690de7f-d0aa-4e9f-a587-d50825f99a56';
  $pane->panel = 'right';
  $pane->type = 'fieldable_panels_pane';
  $pane->subtype = 'fpid:7';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '6690de7f-d0aa-4e9f-a587-d50825f99a56';
  $display->content['new-6690de7f-d0aa-4e9f-a587-d50825f99a56'] = $pane;
  $display->panels['right'][1] = 'new-6690de7f-d0aa-4e9f-a587-d50825f99a56';
  $pane = new stdClass();
  $pane->pid = 'new-1009c41b-3725-4d1a-af37-139b3f65a044';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'webform-client-block-2117';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'panel-webform',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '1009c41b-3725-4d1a-af37-139b3f65a044';
  $display->content['new-1009c41b-3725-4d1a-af37-139b3f65a044'] = $pane;
  $display->panels['right'][2] = 'new-1009c41b-3725-4d1a-af37-139b3f65a044';
  $pane = new stdClass();
  $pane->pid = 'new-c8ec7cfa-649f-4976-bdff-c34e26b4fcbe';
  $pane->panel = 'right_bottom';
  $pane->type = 'fieldable_panels_pane';
  $pane->subtype = 'fpid:6';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'c8ec7cfa-649f-4976-bdff-c34e26b4fcbe';
  $display->content['new-c8ec7cfa-649f-4976-bdff-c34e26b4fcbe'] = $pane;
  $display->panels['right_bottom'][0] = 'new-c8ec7cfa-649f-4976-bdff-c34e26b4fcbe';
  $pane = new stdClass();
  $pane->pid = 'new-d0561d5a-d7e6-4466-858c-c8ef359f0d0a';
  $pane->panel = 'top';
  $pane->type = 'fieldable_panels_pane';
  $pane->subtype = 'fpid:1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => '',
  );
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd0561d5a-d7e6-4466-858c-c8ef359f0d0a';
  $display->content['new-d0561d5a-d7e6-4466-858c-c8ef359f0d0a'] = $pane;
  $display->panels['top'][0] = 'new-d0561d5a-d7e6-4466-858c-c8ef359f0d0a';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-22abdd89-922d-4d3b-be5f-e5ff94425ad5';
  $handler->conf['display'] = $display;
  $export['node_view__panel_context_f6d858e4-2608-4344-9536-8e41878073ec'] = $handler;

  return $export;
}
