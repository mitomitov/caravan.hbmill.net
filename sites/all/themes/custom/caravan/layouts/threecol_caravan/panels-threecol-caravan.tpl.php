<?php

/**

 * @file

 * Template for a 3 column panel layout.

 *

 * This template provides a three column 25%-50%-25% panel display layout, with

 * additional areas for the top and the bottom.

 *

 * Variables:

 * - $id: An optional CSS id to use for the layout.

 * - $content: An array of content, each item in the array is keyed to one

 *   panel of the layout. This layout supports the following sections:

 *   - $content['top']: Content in the top row.

 *   - $content['left']: Content in the left column.

 *   - $content['middle']: Content in the middle column.

 *   - $content['right']: Content in the right column.

 *   - $content['bottom']: Content in the bottom row.

 */

?>

<div class="panel-display panel-3col-caravan clearfix" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <?php if ($content['top']): ?>
  <div class="panel-panel panel-col-top">
    <div class="inside"><?php print $content['top']; ?></div>
  </div>
  <?php endif ?>
  <div class="center-wrapper">
  <?php if (!empty($content['left'])): ?>
    <aside class="panel-panel panel-col-first">
      <div class="inside"><?php print $content['left']; ?></div>
    </aside>
  <?php endif; ?>
    <article class="panel-panel panel-col">
      <div class="inside"><?php print $content['middle']; ?></div>
    </article>
   <?php if (!empty($content['right'])): ?>
    <aside class="panel-panel panel-col-last">
      <div class="inside"><?php print $content['right']; ?></div>
    </aside>
   <?php endif; ?>
  </div>
  <aside class="center-wrapper">
    <?php if (!empty($content['left_bottom'])): ?>
    <div class="panel-panel panel-col-first">
      <div class="inside"><?php print $content['left_bottom']; ?></div>
    </div>
    <?php endif; ?>
    <div class="panel-panel panel-col">
        <div class="panel-col-half"><div class="inside"><?php print $content['middle_bottom_first']; ?></div></div>
        <div class="panel-col-half"><div class="inside"><?php print $content['middle_bottom_second']; ?></div></div>
    </div>
    <?php if (!empty($content['right_bottom'])): ?>
    <div class="panel-panel panel-col-last">
      <div class="inside"><?php print $content['right_bottom']; ?></div>
    </div>
    <?php endif; ?>
  </aside>
</div>
