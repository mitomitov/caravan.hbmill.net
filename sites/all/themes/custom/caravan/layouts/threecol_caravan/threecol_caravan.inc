<?php



// Plugin definition

$plugin = array(

  'title' => t('Three column caravan'),

  'category' => t('Columns: 3'),

  'icon' => 'threecol_caravan.png',

  'theme' => 'panels_threecol_caravan',

  'css' => 'threecol_caravan.css',

  'regions' => array(

    'top' => t('Top'),

    'left' => t('Left side'),

    'middle' => t('Middle column'),

    'right' => t('Right side'),

    'left_bottom' => t('Left side bottom'),

    'middle_bottom_first' => t('Middle column first bottom'),

    'middle_bottom_second' => t('Middle column second bottom'),

    'right_bottom' => t('Right side bottom'),

  ),

);

